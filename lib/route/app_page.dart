import 'package:get/get.dart';
import 'package:getx_login/pages/home.dart';
import 'package:getx_login/pages/login.dart';
import 'package:getx_login/route/app_route.dart';

class AppPage {
  static final pages = [
    GetPage(name: RouteName.home, page: () => const HomePage()),
    GetPage(name: RouteName.login, page: () => const LoginPage())
  ];
}
