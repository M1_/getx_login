import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_login/controller/auth_controller.dart';
import 'package:getx_login/controller/login_controller.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final c = Get.find<LoginController>();
    final auth = Get.find<AuthController>();

    return Scaffold(
      // appBar: AppBar(
      //   title: const Text(
      //     "Login",
      //   ),
      //   centerTitle: true,
      // ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Silahkan Login",
                style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold)),
            const SizedBox(height: 30.0),
            TextField(
              controller: c.emailC,
              textInputAction: TextInputAction.next,
              autocorrect: false,
              keyboardType: TextInputType.emailAddress,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Email'),
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(
              () => TextField(
                controller: c.passwordC,
                autocorrect: false,
                obscureText: c.hidden.value,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                        onPressed: () => c.hidden.toggle(),
                        icon: c.hidden.isTrue
                            ? const Icon(Icons.remove_red_eye)
                            : const Icon(Icons.remove_red_eye_outlined)),
                    border: const OutlineInputBorder(),
                    labelText: 'Password'),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(() => CheckboxListTile(
                  value: c.remember.value,
                  onChanged: (value) => c.remember.toggle(),
                  title: const Text("Ingat saya"),
                  controlAffinity: ListTileControlAffinity.leading,
                )),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () => auth.login(
                    c.emailC.text, c.passwordC.text, c.remember.value),
                child: const Text("Login"))
          ],
        ),
      ),
    );
  }
}
