import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AuthController extends GetxController {
  var isAuth = false.obs;

  final Map<String, String> _userData = {
    'email': 'admin@gmail.com',
    'password': '1234567'
  };

  void dialogError(String msg) {
    Get.defaultDialog(title: "Kesalahan!!!", middleText: msg);
  }

  void login(String email, String password, bool rememberme) {
    if (email != '' && password != '') {
      if (GetUtils.isEmail(email)) {
        if (email == _userData['email'] && password == _userData['password']) {
          if (rememberme) {
            //simpan data login dengan get storage
            final box = GetStorage();
            box.write('loginData', {
              "email": email,
              "password": password,
              "rememberme": rememberme
            });
          } else {
            //hapus data login
            final box = GetStorage();
            if (box.read('loginData') != null) {
              box.erase();
            }
          }
          isAuth.value = true;
        } else {
          dialogError("data tidak valid");
        }
      } else {
        dialogError("Email tidak valid");
      }
    } else {
      dialogError("Data tidak boleh kosong");
    }
  }

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    await GetStorage.init();
  }
}
