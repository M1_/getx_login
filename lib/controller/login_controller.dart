import 'package:get/get.dart';
import 'package:flutter/material.dart';

class LoginController extends GetxController {
  var hidden = true.obs;
  var remember = false.obs;
  void gantimode() => hidden.value = !hidden.value;
  late TextEditingController emailC;
  late TextEditingController passwordC;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    emailC = TextEditingController();
    passwordC = TextEditingController();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    emailC.dispose();
    passwordC.dispose();
    super.onClose();
  }
}
