import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_login/controller/auth_controller.dart';
import 'package:getx_login/pages/home.dart';
import 'package:getx_login/pages/login.dart';
import 'package:getx_login/route/app_page.dart';

import 'controller/login_controller.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final loginC = Get.put(LoginController());
    final authC = Get.put(AuthController());
    return Obx(() => GetMaterialApp(
          home: authC.isAuth.isTrue ? const HomePage() : const LoginPage(),
          getPages: AppPage.pages,
        ));
  }
}
